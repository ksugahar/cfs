<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/simulation"
  xmlns="http://www.cfs++.org/simulation"
  xmlns:cfs="http://www.cfs++.org/simulation"
  elementFormDefault="qualified">

  <xsd:annotation>
    <xsd:documentation xml:lang="en">
      Coupled Field Solver project CFS++
      Schema for PDE description for a magnetic PDE
    </xsd:documentation>
  </xsd:annotation>


  <!-- ******************************************************************* -->
  <!--   Definition of element for magnetics PDEs -->
  <!-- ******************************************************************* -->
  <xsd:element name="magnetic" type="DT_PDEMagnetic" substitutionGroup="PDEBasic">
    <xsd:annotation>
      <xsd:documentation>Solves for the magnetic field in 2D (3D is nor recommended; use in 3D magEdgePDE, which applies edge elements);
        standarrd nodal finite elements are used; primary dof is the magnetic vector potential.
        There are the following formulations available:
        -) A: magnetic vector potential discretized with nodal basis functions (B-based formulation)
        -) Psi: reduced magnetic scalar potential discretized with nodal basis functions (H-based formulation)
      </xsd:documentation>
    </xsd:annotation>
    <xsd:unique name="CS_MagneticRegion">
      <xsd:selector xpath="cfs:region"/>
      <xsd:field xpath="@name"/>
    </xsd:unique>
  </xsd:element>


  <!-- ******************************************************************* -->
  <!--   Definition of data type for magnetics PDEs -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_PDEMagnetic">
    <xsd:complexContent>
      <xsd:extension base="DT_PDEBasic">
        <xsd:sequence>

          <!-- Regions the PDE lives on -->
          <xsd:element name="regionList" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="region" minOccurs="1" maxOccurs="unbounded">
                  <xsd:complexType>
                    <xsd:attribute name="name" type="xsd:token" use="required"/>
                    <xsd:attribute name="polyId" type="xsd:string" use="optional" default="default"> </xsd:attribute>
                    <xsd:attribute name="integId" type="xsd:string" use="optional" default="default"> </xsd:attribute>
                    <xsd:attribute name="nonLinIds" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="velocityId" type="xsd:token" use="optional" default=""/>
                  </xsd:complexType>
                </xsd:element>
              </xsd:sequence>
            </xsd:complexType>
          </xsd:element>

          <!-- List defining nonlinear types -->
          <xsd:element name="ncInterfaceList" type="DT_NcInterfaceList" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation>Definnes the non-matching interfaces</xsd:documentation>
            </xsd:annotation>
          </xsd:element>

          <xsd:element name="nonLinList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
		    <xsd:element name="reluctivity_magstrict" type="DT_MagneticNonLinReluc"/>  

                <xsd:element name="permeability" type="DT_MagneticNonLinPerm">
                  <xsd:annotation>
                    <xsd:documentation>Permeability depends on magnetic flux density</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                 <xsd:element name="hysteresis" type="DT_ElecNonLinHyst">
                  <xsd:annotation>
                    <xsd:documentation>Permeability depends on magnetic flux density</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
              </xsd:choice>

            </xsd:complexType>
          </xsd:element>
          
          <!-- Type of velocity (optional) -->
          <xsd:element name="velocityList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="velocity" type="DT_BcInhomVector"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Initial conditions (optional) -->
          <xsd:element name="initialValues" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">

                <!-- Initial state of previous sequence step / external file -->
                <xsd:element name="initialState" type="DT_InitialState" minOccurs="0" maxOccurs="1"/>

              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Boundary Conditions & Loads (optional) -->
          <xsd:element name="bcsAndLoads" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <!-- Dirichlet Boundary Conditions -->
                <xsd:element name="fluxParallel" type="DT_BcHomVector">
                  <xsd:annotation>
                    <xsd:documentation>Forces magnetic flux lines to be parallel to boundary</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <xsd:element name="ground" type="DT_BcHomScalar">
                  <xsd:annotation>
                    <xsd:documentation>Sets all components of magnetic vector potential to zero</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <xsd:element name="potential" type="DT_BcInhomScalar">
                  <xsd:annotation>
                    <xsd:documentation>Prescribes the magnetic vector potential</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <xsd:element name="elecPotential" type="DT_BcInhomScalar">
                  <xsd:annotation>
                    <xsd:documentation>Currently not used!!!!</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>

                <!-- RHS Load Values-->
                <xsd:element name="fluxDensity" type="DT_BcInhomVector">
                  <xsd:annotation>
                    <xsd:documentation>Prescribes the magnetic flux density</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <xsd:element name="fieldIntensity" type="DT_BcInhomVector">
                  <xsd:annotation>
                    <xsd:documentation>Exciting magnetic field intensity precomupted by curl-curl formulation</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <!-- for iterative coupling to magnetostrictive material -->
                <xsd:element name="mechStrain"      type="DT_BcInhomVector"    />
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>


          <!-- Section for specifying the Biot-Savart Law (optional) -->
          <xsd:element name="biotSavart" type="DT_BiotSavartCoil" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation>Computes the magnetic field generated by coils in air; can be used in a superposition mode!</xsd:documentation>
            </xsd:annotation>
          </xsd:element>

          <!-- Section for specifying the coils (optional) -->
          <xsd:element name="coilList" minOccurs="0">
            <xsd:annotation>
              <xsd:documentation>Defines the coil parameters</xsd:documentation>
            </xsd:annotation>
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="coil" type="DT_MagCoil" minOccurs="0" maxOccurs="unbounded"/>
              </xsd:sequence>
            </xsd:complexType>
          </xsd:element>

          <!-- Desired solution values (optional) -->
          <xsd:element name="storeResults" type="DT_MagneticStoreResults" minOccurs="0"
            maxOccurs="1"/>
        </xsd:sequence>
        <xsd:attribute name="type" use="optional" default="lagrange">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="lagrange"/>
              <xsd:enumeration value="legendre"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
        <xsd:attribute name="order" use="optional" type="xsd:positiveInteger" default="1"/>

        <xsd:attribute name="formulation" use="optional" default="A">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="A"/>
              <xsd:enumeration value="Psi"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute> 

      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of reluctivity nonlinearity -->
  <!-- ******************************************************************* -->

  <!-- Definition of permeability nonlinearity type -->
  <xsd:complexType name="DT_MagneticNonLinReluc">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of permeability nonlinearity -->
  <!-- ******************************************************************* -->

  <!-- Definition of permeability nonlinearity type -->
  <xsd:complexType name="DT_MagneticNonLinPerm">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of hysteresis nonlinearity -->
  <!-- ******************************************************************* -->

  <!-- Definition of hysteresis nonlinearity type -->
  <xsd:complexType name="DT_MagNonLinHyst">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> 
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of the magnetic unknown types -->
  <!-- ******************************************************************* -->
  <xsd:simpleType name="DT_MagneticUnknownType">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magPotential"/>
      <xsd:enumeration value="elecPotential"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- ******************************************************************* -->
  <!--   Definition of enumeration type describing the degrees of freedom  -->
  <!-- ******************************************************************* -->

  <xsd:simpleType name="DT_MagneticDOF">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="x"/>
      <xsd:enumeration value="y"/>
      <xsd:enumeration value="z"/>
      <xsd:enumeration value="phi"/>
      <xsd:enumeration value="r"/>
      <xsd:enumeration value=""/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- ******************************************************************* -->
  <!--   Definition of the boundary condition types for magnetics -->
  <!-- ******************************************************************* -->

  <!-- Element type for homogeneous Dirichlet boundary conditions -->
  <!-- We derive it by extending the base type -->
  <xsd:complexType name="DT_MagneticHD">
    <xsd:complexContent>
      <xsd:extension base="DT_BCBasic">
        <xsd:attribute name="name" type="xsd:token" use="required"/>
        <xsd:attribute name="dof" type="DT_MagneticDOF" use="optional" default=""/>
        <xsd:attribute name="quantity" default="magPotential" type="DT_MagneticUnknownType"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for inhomogeneous Dirichlet boundary conditions -->
  <!-- We derive it by extending the homogeneous case -->
  <xsd:complexType name="DT_MagneticID">
    <xsd:complexContent>
      <xsd:extension base="DT_MagneticHD">
        <xsd:attribute name="value" type="xsd:token" use="required"/>
        <xsd:attribute name="phase" type="xsd:token" use="optional" default="0.0"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for specifying inhomogeneous neumann conditions -->
  <!-- Identical to that of inhomogeneous Dirichlet case -->
  <xsd:complexType name="DT_MagneticIN">
    <xsd:complexContent>
      <xsd:extension base="DT_MagneticID"/>
    </xsd:complexContent>
  </xsd:complexType>


  <!-- Element type for constraint condition -->
  <!-- We derive it by extending the base type -->
  <xsd:complexType name="DT_MagneticCS">
    <xsd:complexContent>
      <xsd:extension base="DT_BCBasic">
        <xsd:attribute name="name" type="xsd:token" use="required"/>
        <xsd:attribute name="quantity" default="magPotential" type="DT_MagneticUnknownType"/>
        <xsd:attribute name="masterDof" type="xsd:token" use="optional" default=""/>
        <xsd:attribute name="slaveDof" type="xsd:token" use="optional" default=""/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>


  <!-- ******************************************************************* -->
  <!--   Definition of data type for non-linearity in mechanic PDEs -->
  <!-- ******************************************************************* -->
  <xsd:simpleType name="DT_MagneticNonLin">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="no"/>
      <xsd:enumeration value="geo"/>
      <xsd:enumeration value="permeability"/>
      <xsd:enumeration value="reluctivity_magstrict"/>
      <xsd:enumeration value="conductivity"/>
    </xsd:restriction>
  </xsd:simpleType>


  <!-- ******************************************************************* -->
  <!--   Definition of syntax for specifying output quantities of CFS -->
  <!-- ******************************************************************* -->

  <!-- Definition of nodal result types of magnetic PDE -->
  <xsd:simpleType name="DT_MagneticNodeResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magPotential"/>
      <xsd:enumeration value="magRhsLoad"/>
      <xsd:enumeration value="elecPotential"/>
      <xsd:enumeration value="lagrangeMultiplier"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of elem result types of magnetic PDE -->
  <xsd:simpleType name="DT_MagneticElemResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magFluxDensity"/>
      <xsd:enumeration value="magFieldIntensity"/>
      <xsd:enumeration value="elecFieldIntensity"/>
      <xsd:enumeration value="magEddyCurrentDensity"/>
      <xsd:enumeration value="magCoilCurrentDensity"/>
      <xsd:enumeration value="magTotalCurrentDensity"/>
      <xsd:enumeration value="magForceLorentzDensity"/>
      <xsd:enumeration value="magEddyPowerDensity"/>
      <xsd:enumeration value="magEnergyDensity"/>
      <xsd:enumeration value="fluxIndStrain"/>
      <xsd:enumeration value="magCoreLossDensity"/>
      <xsd:enumeration value="magMagnetization"/>
      <xsd:enumeration value="magPolarization"/>
      <xsd:enumeration value="magElemPermeability"/>
      <xsd:enumeration value="magJouleLossPowerDensity"/>
      
      <!-- non-physical design variable for simp topology optimization of magnetic material (standard case) -->
      <xsd:enumeration value="pseudoDensity" />
      <!-- physical application of design variable in the simulation -->
      <xsd:enumeration value="physicalPseudoDensity" />
      <!-- topology optimization for coils - the variable itself for non-magnetic materials, e.g. copper -->
      <xsd:enumeration value="rhsPseudoDensity" />
      <!-- physical (filtered) variant -->
      <xsd:enumeration value="physicalRhsPseudoDensity" />
      <!-- further element results from optimization, specifically configured in optimization --> 
      <xsd:enumeration value="optResult_1"/>
      <xsd:enumeration value="optResult_2"/>
      <xsd:enumeration value="optResult_3"/>
      <xsd:enumeration value="optResult_4"/>
      <xsd:enumeration value="optResult_5"/>
      <xsd:enumeration value="optResult_6"/>
      <xsd:enumeration value="optResult_7"/>
      <xsd:enumeration value="optResult_8"/>
      <xsd:enumeration value="optResult_9"/>
    </xsd:restriction>
  </xsd:simpleType>


  <!-- Definition of field variables (union of nodal and element results) -->
  <xsd:simpleType name="DT_MagneticSensorArrayResult">
    <xsd:union memberTypes="DT_MagneticNodeResult DT_MagneticElemResult"/>
  </xsd:simpleType>

  <!-- Definition of region result types of magnetic PDE -->
  <xsd:simpleType name="DT_MagneticRegionResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magEnergy"/>
      <xsd:enumeration value="magEddyPower"/>
      <xsd:enumeration value="magForceLorentz"/>
      <xsd:enumeration value="magCoreLoss"/>
      <xsd:enumeration value="magEddyCurrent"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of surface element result types of electrostatic PDE -->
  <xsd:simpleType name="DT_MagneticSurfElemResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magForceMaxwellDensity"/>
      <xsd:enumeration value="magNormalForceMaxwellDensity"/>
      <xsd:enumeration value="magTangentialForceMaxwellDensity"/>
    </xsd:restriction>
  </xsd:simpleType>
  
  <!-- Definition of surf region result types of magnetic PDE -->
  <xsd:simpleType name="DT_MagneticSurfRegionResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="magFlux"/>
      <xsd:enumeration value="magForceMaxwell"/>
      <xsd:enumeration value="magForceVWP"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of coil result types of magnetic PDE -->
  <xsd:simpleType name="DT_MagneticCoilResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="coilInductance"/>
      <xsd:enumeration value="coilCurrent"/>
      <xsd:enumeration value="coilInducedVoltage"/>
      <xsd:enumeration value="coilLinkedFlux"/>
    </xsd:restriction>
  </xsd:simpleType>


  <!-- Global type for specifying desired electrostatic output quantities -->
  <xsd:complexType name="DT_MagneticStoreResults">
    <xsd:sequence>
      <xsd:choice maxOccurs="unbounded">

        <!-- Nodal result definition -->
        <xsd:element name="nodeResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_NodeResult">
                <xsd:attribute name="type" type="DT_MagneticNodeResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Element result definition -->
        <xsd:element name="elemResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_ElemResult">
                <xsd:attribute name="type" type="DT_MagneticElemResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Region result definition -->
        <xsd:element name="regionResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_RegionResult">
                <xsd:attribute name="type" type="DT_MagneticRegionResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Coil result definition -->
        <xsd:element name="coilResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_CoilResult">
                <xsd:attribute name="type" type="DT_MagneticCoilResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- SensorArrayResults -->
        <xsd:element name="sensorArray" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SensorArrayResult">
                <xsd:attribute name="type" type="DT_MagneticSensorArrayResult"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Surface element result definition -->
        <xsd:element name="surfElemResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SurfElemResult">
                <xsd:attribute name="type" type="DT_MagneticSurfElemResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>
        
        <!-- Surface region result definition -->
        <xsd:element name="surfRegionResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SurfRegionResult">
                <xsd:attribute name="type" type="DT_MagneticSurfRegionResult"
                  use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

      </xsd:choice>
    </xsd:sequence>
  </xsd:complexType>

</xsd:schema>
