cmake_minimum_required(VERSION 2.8)

project(minizip C)

SET(MINIZIP_VERSION "1.1")

set(MINIZIP_BUILD_SHARED "ON" CACHE BOOL "Build a shared version of the library")

SET(ZLIB_INCLUDE_DIR /usr/include
  CACHE PATH
  "Include directory for zlib.")
SET(ZLIB_LIBRARY /usr/lib/libz.so
  CACHE FILEPATH
  "Zlib library.")

if(CMAKE_HOST_APPLE)
  set(PLATFORM __APPLE__)
elseif(CMAKE_HOST_UNIX)
  set(PLATFORM unix)
elseif(CMAKE_HOST_WIN32)
  set(PLATFORM _WIN32)
else(CMAKE_HOST_APPLE)
  message(FATAL_ERROR "Not supported Platform")
endif(CMAKE_HOST_APPLE)

add_definitions(-D${PLATFORM} -DNOCRYPT)

if(CMAKE_COMPILER_IS_GNUCC)
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
endif()

include_directories("${ZLIB_INCLUDE_DIR}")
set(SOURCE
  ioapi.c
  mztools.c
  unzip.c
  zip.c
)

if(WIN32)
  LIST(APPEND SOURCE iowin32.c)
endif(WIN32)

set(HEADERS
  crypt.h
  ioapi.h
  mztools.h
  unzip.h
  zip.h
)

if(WIN32)
  LIST(APPEND HEADERS iowin32.h)
endif(WIN32)

add_library(minizip_static STATIC ${SOURCE} ${HEADERS})

IF(MINIZIP_BUILD_SHARED)
  add_library(minizip_shared SHARED ${SOURCE} ${HEADERS})

  # under windows we need to define ZLIB_DLL
  if(WIN32)
    set_target_properties(minizip_shared PROPERTIES COMPILE_FLAGS "-DZLIB_DLL -D_WIN32_WINNT=0x0602")
    set_target_properties(minizip_static PROPERTIES COMPILE_FLAGS -D_WIN32_WINNT=0x0602)
  endif()

  target_link_libraries(minizip_shared ${ZLIB_LIBRARY})

  # for windows we need to change the name of the shared library
  # for both static and shared version to exist
  if(WIN32)
    set_target_properties(minizip_shared PROPERTIES OUTPUT_NAME minizipdll)
  else()
    set_target_properties(minizip_shared PROPERTIES OUTPUT_NAME minizip)
  endif()
  set_target_properties(minizip_shared PROPERTIES CLEAN_DIRECT_OUTPUT 1)
ENDIF()

set_target_properties(minizip_static PROPERTIES VERSION ${MINIZIP_VERSION})
set_target_properties(minizip_static PROPERTIES SOVERSION ${MINIZIP_VERSION})

if(MINIZIP_BUILD_SHARED)
  set_target_properties(minizip_shared PROPERTIES VERSION ${MINIZIP_VERSION})
  set_target_properties(minizip_shared PROPERTIES SOVERSION ${MINIZIP_VERSION})
endif()

# Set the install path of the static library
install(TARGETS minizip_static ARCHIVE DESTINATION ${LIB_SUFFIX})
# Set the install path of the shared library
if(MINIZIP_BUILD_SHARED)
  # for windows, need to install both minizipdll.dll and minizipdll.lib
  if(WIN32)
    install(TARGETS minizip_shared ARCHIVE DESTINATION ${LIB_SUFFIX})
    install(TARGETS minizip_shared RUNTIME DESTINATION bin)
  else()
    install(TARGETS minizip_shared LIBRARY DESTINATION ${LIB_SUFFIX})
  endif()
endif(MINIZIP_BUILD_SHARED)

# Set the install path of the header files
install(FILES
  ${HEADERS}
  DESTINATION include/minizip)
